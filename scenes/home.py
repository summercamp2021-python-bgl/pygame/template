from typing import Any
from scene import Scene

import pygame as game
import pygame.event as event

sceneName = "home"
class HomeScene(Scene):
    def __init__(self) -> None:
        super().__init__()

    def init(self, surface: game.Surface) -> None:
        return super().init(surface)

    def onOpen(self, surface: game.Surface, arg: Any) -> None:
        return super().onOpen(surface,arg)
    
    def logic(self, surface: game.Surface, dT: float) -> bool:
        return super().logic(surface, dT)

    def render(self, surface: game.Surface) -> None:
        return super().render(surface)

    def handleEvent(self, event: event.Event) -> None:
        return super().handleEvent(event)
    
    def onBackground(self, surface: game.Surface, arg: Any) -> None:
        return super().onBackground(surface,arg)

    def onClose(self, surface: game.Surface, arg: Any) -> None:
        return super().onClose(surface,arg)